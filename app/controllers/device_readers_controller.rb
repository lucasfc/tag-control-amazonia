class DeviceReadersController < ApplicationController
  before_action :set_device_reader, only: [:show, :update, :destroy]

  # GET /device_readers
  def index
    @device_readers = DeviceReader.all

    render json: @device_readers
  end

  # GET /device_readers/1
  def show
    render json: @device_reader
  end

  # POST /device_readers
  def create
    @device_reader = DeviceReader.new(device_reader_params)

    if @device_reader.save
      render json: @device_reader, status: :created, location: @device_reader
    else
      render json: @device_reader.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /device_readers/1
  def update
    if @device_reader.update(device_reader_params)
      render json: @device_reader
    else
      render json: @device_reader.errors, status: :unprocessable_entity
    end
  end

  # DELETE /device_readers/1
  def destroy
    @device_reader.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_device_reader
      @device_reader = DeviceReader.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def device_reader_params
      params.require(:device_reader).permit(:name, :code, :secrect, :description)
    end
end
