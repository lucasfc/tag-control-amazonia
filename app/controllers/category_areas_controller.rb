class CategoryAreasController < ApplicationController
  before_action :set_category_area, only: [:show, :update, :destroy]

  # GET /category_areas
  def index
    @category_areas = CategoryArea.all

    render json: @category_areas
  end

  # GET /category_areas/1
  def show
    render json: @category_area
  end

  # POST /category_areas
  def create
    @category_area = CategoryArea.new(category_area_params)

    if @category_area.save
      render json: @category_area, status: :created, location: @category_area
    else
      render json: @category_area.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /category_areas/1
  def update
    if @category_area.update(category_area_params)
      render json: @category_area
    else
      render json: @category_area.errors, status: :unprocessable_entity
    end
  end

  # DELETE /category_areas/1
  def destroy
    @category_area.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category_area
      @category_area = CategoryArea.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def category_area_params
      params.require(:category_area).permit(:category_id, :area_id)
    end
end
