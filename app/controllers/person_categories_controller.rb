class PersonCategoriesController < ApplicationController
  before_action :set_person_category, only: [:show, :update, :destroy]

  # GET /person_categories
  def index
    @person_categories = PersonCategory.all

    render json: @person_categories
  end

  # GET /person_categories/1
  def show
    render json: @person_category
  end

  # POST /person_categories
  def create
    @person_category = PersonCategory.new(person_category_params)

    if @person_category.save
      render json: @person_category, status: :created, location: @person_category
    else
      render json: @person_category.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /person_categories/1
  def update
    if @person_category.update(person_category_params)
      render json: @person_category
    else
      render json: @person_category.errors, status: :unprocessable_entity
    end
  end

  # DELETE /person_categories/1
  def destroy
    @person_category.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person_category
      @person_category = PersonCategory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def person_category_params
      params.require(:person_category).permit(:person_id, :category_id)
    end
end
