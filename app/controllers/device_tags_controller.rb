class DeviceTagsController < ApplicationController
  before_action :set_device_tag, only: [:show, :update, :destroy]

  # GET /device_tags
  def index
    @device_tags = DeviceTag.all

    render json: @device_tags
  end

  # GET /device_tags/1
  def show
    render json: @device_tag
  end

  # POST /device_tags
  def create
    @device_tag = DeviceTag.new(device_tag_params)

    if @device_tag.save
      render json: @device_tag, status: :created, location: @device_tag
    else
      render json: @device_tag.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /device_tags/1
  def update
    if @device_tag.update(device_tag_params)
      render json: @device_tag
    else
      render json: @device_tag.errors, status: :unprocessable_entity
    end
  end

  # DELETE /device_tags/1
  def destroy
    @device_tag.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_device_tag
      @device_tag = DeviceTag.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def device_tag_params
      params.require(:device_tag).permit(:code, :description)
    end
end
