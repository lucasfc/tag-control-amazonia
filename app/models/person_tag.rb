class PersonTag < ApplicationRecord
  belongs_to :person
  belongs_to :device_tag

  after_create :send_whatsapp

  def self.deactive(tag)
    device = DeviceTag.find_by_code(tag)
    if device.blank?
      return {status: false, mensagem: "Este dispositivo não está cadastrado"}
    end

    pt = PersonTag.find_by(device_tag: device, active:true)

    if pt.blank?
      return {status: false, mensagem: "Este dispositivo não está ativado"}
    else
      pt.inactivated_at = DateTime.now
      pt.active= false
      pt.save
      return {status: true, mensagem: "Dispositivo Devolvido Com Sucesso"}
    end
  end

  def send_whatsapp
    begin
      if person.phone.present?
        first_name = person.name.split(' ').first
        phone = "+55#{person.phone.gsub(/\D/, '')}"
        params = {
            "message": "#{first_name}, estamos muito felizes por ter vindo participar das consultas públicas do PPA. Como retibuição à sua vinda, nós temos uma super novidade para você. Entre em https://w.blzi.me/ppa/u para saber mais.",
            "reference": "#{self.id}",
            "priority": "normal",
            "phone": phone,
            "retentionPolicy": "plan_defaults"
        }

        url = URI("https://api.wassenger.com/v1/messages")

        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE

        request = Net::HTTP::Post.new(url)
        request["content-type"] = 'application/json'
        request["token"] = 'ad0a6d1dd1aae387d5e71eedd1013e9b0a36ca733e6e394bacc73dae9bad36eda55b435523d45fbb'
        request.body = params.to_json

        response = http.request(request)
        puts response.read_body
      end
    rescue
    end
  end
end
