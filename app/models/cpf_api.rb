class CpfApi

    #Configura a url do provedor que retorna os dados de CPF
    def self.provider
      "https://api.cpfcnpj.com.br"
    end

    #Configura o token do provedor
    def self.token
      "401dbc5dbf435f8ae6ee0a8e00ebacef"
    end

    #Retorna os dados de acordo com o CPF informado quando houver conexão com a internet
    #Caso não haja conexão, ou o CPF for inválido, retorna vazio
    def self.get_data(cpf)
      if check_cpf(cpf)
        url = "#{provider}/#{token}/1/json/#{cpf}"
        begin
          client = RestClient.get(url)
          JSON.parse(client, symbolize_names: true)
        rescue
          {}
        end
      else
      end
    end

    #Verifica se o CPF informado é válido.
    #==== Exemplos
    #
    #CpfApi.check_cpf() # => false
    #CpfApi.check_cpf('11111111111') # => false
    #CpfApi.check_cpf('12345678909') # => true
    def self.check_cpf(cpf=nil)
      return false if cpf.nil?
      nulos = %w{12345678909 11111111111 22222222222 33333333333 44444444444 55555555555 66666666666 77777777777 88888888888 99999999999 00000000000}
      valor = cpf.scan /[0-9]/
      if valor.length == 11
        unless nulos.member?(valor.join)
          valor = valor.collect{|x| x.to_i}
          soma = 10*valor[0]+9*valor[1]+8*valor[2]+7*valor[3]+6*valor[4]+5*valor[5]+4*valor[6]+3*valor[7]+2*valor[8]
          soma = soma - (11 * (soma/11))
          resultado1 = (soma == 0 or soma == 1) ? 0 : 11 - soma
          if resultado1 == valor[9]
            soma = valor[0]*11+valor[1]*10+valor[2]*9+valor[3]*8+valor[4]*7+valor[5]*6+valor[6]*5+valor[7]*4+valor[8]*3+valor[9]*2
            soma = soma - (11 * (soma/11))
            resultado2 = (soma == 0 or soma == 1) ? 0 : 11 - soma
            return true if resultado2 == valor[10] # CPF válido
          end
        end
      end
      return false # CPF inválido
    end

end