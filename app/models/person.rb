class Person < ApplicationRecord
  has_many :person_tags
  has_many :person_categories
  has_many :categories, through: :person_categories
  has_many :areas, through: :categories

  validates :idnumber, uniqueness: true

  def self.create_by_ppa(params)
    person_params= JSON.parse(params[:person], symbolize_names: true)
    person = find_or_create_by(idnumber: person_params[:idnumber])
    if person_params[:category_ids].blank?
      person_params[:category_ids] = person.category_ids
    end
    person.update(
        person_params
    )

    person.sync_in_device(params[:tag])
  end

  def sync_in_device(code)
    device = DeviceTag.find_or_create_by(code: code)
    last_rel = PersonTag.where(active: true, device_tag: device)
    if last_rel.present?
      last_rel.update_all(active: false, inactivated_at: DateTime.now)
    end
    PersonTag.create(
                 active: true,
                 person: self,
                 device_tag: device
    )
  end

  def self.new_by_api(cpf)
    begin
    #data = CpfApi.get_data(cpf)
    Person.create(
        #name: data[:nome],
        idnumber: cpf,
    )
    rescue
      Person.create(idnumber: cpf)
    end
  end
end
