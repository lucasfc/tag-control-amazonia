class Category < ApplicationRecord
  has_many :category_areas
  has_many :areas, through: :category_areas
end
