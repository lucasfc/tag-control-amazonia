require 'test_helper'

class DeviceTagsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @device_tag = device_tags(:one)
  end

  test "should get index" do
    get device_tags_url, as: :json
    assert_response :success
  end

  test "should create device_tag" do
    assert_difference('DeviceTag.count') do
      post device_tags_url, params: { device_tag: { code: @device_tag.code, description: @device_tag.description } }, as: :json
    end

    assert_response 201
  end

  test "should show device_tag" do
    get device_tag_url(@device_tag), as: :json
    assert_response :success
  end

  test "should update device_tag" do
    patch device_tag_url(@device_tag), params: { device_tag: { code: @device_tag.code, description: @device_tag.description } }, as: :json
    assert_response 200
  end

  test "should destroy device_tag" do
    assert_difference('DeviceTag.count', -1) do
      delete device_tag_url(@device_tag), as: :json
    end

    assert_response 204
  end
end
