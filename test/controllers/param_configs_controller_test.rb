require 'test_helper'

class ParamConfigsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @param_config = param_configs(:one)
  end

  test "should get index" do
    get param_configs_url, as: :json
    assert_response :success
  end

  test "should create param_config" do
    assert_difference('ParamConfig.count') do
      post param_configs_url, params: { param_config: { namespace: @param_config.namespace, value: @param_config.value } }, as: :json
    end

    assert_response 201
  end

  test "should show param_config" do
    get param_config_url(@param_config), as: :json
    assert_response :success
  end

  test "should update param_config" do
    patch param_config_url(@param_config), params: { param_config: { namespace: @param_config.namespace, value: @param_config.value } }, as: :json
    assert_response 200
  end

  test "should destroy param_config" do
    assert_difference('ParamConfig.count', -1) do
      delete param_config_url(@param_config), as: :json
    end

    assert_response 204
  end
end
