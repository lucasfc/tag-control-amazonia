require 'test_helper'

class PersonCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @person_category = person_categories(:one)
  end

  test "should get index" do
    get person_categories_url, as: :json
    assert_response :success
  end

  test "should create person_category" do
    assert_difference('PersonCategory.count') do
      post person_categories_url, params: { person_category: { category_id: @person_category.category_id, person_id: @person_category.person_id } }, as: :json
    end

    assert_response 201
  end

  test "should show person_category" do
    get person_category_url(@person_category), as: :json
    assert_response :success
  end

  test "should update person_category" do
    patch person_category_url(@person_category), params: { person_category: { category_id: @person_category.category_id, person_id: @person_category.person_id } }, as: :json
    assert_response 200
  end

  test "should destroy person_category" do
    assert_difference('PersonCategory.count', -1) do
      delete person_category_url(@person_category), as: :json
    end

    assert_response 204
  end
end
