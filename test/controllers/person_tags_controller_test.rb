require 'test_helper'

class PersonTagsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @person_tag = person_tags(:one)
  end

  test "should get index" do
    get person_tags_url, as: :json
    assert_response :success
  end

  test "should create person_tag" do
    assert_difference('PersonTag.count') do
      post person_tags_url, params: { person_tag: { active: @person_tag.active, device_tag_id: @person_tag.device_tag_id, inactivated_at: @person_tag.inactivated_at, person_id: @person_tag.person_id } }, as: :json
    end

    assert_response 201
  end

  test "should show person_tag" do
    get person_tag_url(@person_tag), as: :json
    assert_response :success
  end

  test "should update person_tag" do
    patch person_tag_url(@person_tag), params: { person_tag: { active: @person_tag.active, device_tag_id: @person_tag.device_tag_id, inactivated_at: @person_tag.inactivated_at, person_id: @person_tag.person_id } }, as: :json
    assert_response 200
  end

  test "should destroy person_tag" do
    assert_difference('PersonTag.count', -1) do
      delete person_tag_url(@person_tag), as: :json
    end

    assert_response 204
  end
end
