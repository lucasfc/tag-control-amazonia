require 'test_helper'

class CategoryAreasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @category_area = category_areas(:one)
  end

  test "should get index" do
    get category_areas_url, as: :json
    assert_response :success
  end

  test "should create category_area" do
    assert_difference('CategoryArea.count') do
      post category_areas_url, params: { category_area: { area_id: @category_area.area_id, category_id: @category_area.category_id } }, as: :json
    end

    assert_response 201
  end

  test "should show category_area" do
    get category_area_url(@category_area), as: :json
    assert_response :success
  end

  test "should update category_area" do
    patch category_area_url(@category_area), params: { category_area: { area_id: @category_area.area_id, category_id: @category_area.category_id } }, as: :json
    assert_response 200
  end

  test "should destroy category_area" do
    assert_difference('CategoryArea.count', -1) do
      delete category_area_url(@category_area), as: :json
    end

    assert_response 204
  end
end
