require 'test_helper'

class ReplyPeopleControllerTest < ActionDispatch::IntegrationTest
  setup do
    @reply_person = reply_people(:one)
  end

  test "should get index" do
    get reply_people_url, as: :json
    assert_response :success
  end

  test "should create reply_person" do
    assert_difference('ReplyPerson.count') do
      post reply_people_url, params: { reply_person: { person_id: @reply_person.person_id, reply_id: @reply_person.reply_id } }, as: :json
    end

    assert_response 201
  end

  test "should show reply_person" do
    get reply_person_url(@reply_person), as: :json
    assert_response :success
  end

  test "should update reply_person" do
    patch reply_person_url(@reply_person), params: { reply_person: { person_id: @reply_person.person_id, reply_id: @reply_person.reply_id } }, as: :json
    assert_response 200
  end

  test "should destroy reply_person" do
    assert_difference('ReplyPerson.count', -1) do
      delete reply_person_url(@reply_person), as: :json
    end

    assert_response 204
  end
end
