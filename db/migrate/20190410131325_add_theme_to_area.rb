class AddThemeToArea < ActiveRecord::Migration[5.1]
  def change
    add_column :areas, :theme_id, :integer
    add_column :areas, :event_id, :integer
  end
end
