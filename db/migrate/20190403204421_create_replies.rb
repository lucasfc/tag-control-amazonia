class CreateReplies < ActiveRecord::Migration[5.1]
  def change
    create_table :replies do |t|
      t.references :question, foreign_key: true
      t.string :code
      t.string :name

      t.timestamps
    end
  end
end
