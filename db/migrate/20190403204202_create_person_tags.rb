class CreatePersonTags < ActiveRecord::Migration[5.1]
  def change
    create_table :person_tags do |t|
      t.references :person, foreign_key: true
      t.references :device_tag, foreign_key: true
      t.datetime :inactivated_at
      t.boolean :active

      t.timestamps
    end
  end
end
