class CreateParamConfigs < ActiveRecord::Migration[5.1]
  def change
    create_table :param_configs do |t|
      t.string :namespace
      t.string :value

      t.timestamps
    end
  end
end
