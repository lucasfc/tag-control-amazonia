class CreateReplyPeople < ActiveRecord::Migration[5.1]
  def change
    create_table :reply_people do |t|
      t.references :reply, foreign_key: true
      t.references :person, foreign_key: true

      t.timestamps
    end
  end
end
