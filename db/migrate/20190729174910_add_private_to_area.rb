class AddPrivateToArea < ActiveRecord::Migration[5.1]
  def change
    add_column :areas, :private, :boolean
  end
end
