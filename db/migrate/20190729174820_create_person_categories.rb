class CreatePersonCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :person_categories do |t|
      t.references :person, foreign_key: true
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
