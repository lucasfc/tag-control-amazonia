class CreateCategoryAreas < ActiveRecord::Migration[5.1]
  def change
    create_table :category_areas do |t|
      t.references :category, foreign_key: true
      t.references :area, foreign_key: true

      t.timestamps
    end
  end
end
