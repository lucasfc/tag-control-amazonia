class CreatePeople < ActiveRecord::Migration[5.1]
  def change
    create_table :people do |t|
      t.string :name
      t.string :idnumber
      t.string :email
      t.string :phone
      t.string :company
      t.string :role
      t.date :date
      t.string :mother
      t.string :gender
      t.text :photo

      t.timestamps
    end
  end
end
