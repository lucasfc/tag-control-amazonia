class CreateTagRegisters < ActiveRecord::Migration[5.1]
  def change
    create_table :tag_registers do |t|
      t.references :person_tag, foreign_key: true
      t.references :device_reader, foreign_key: true
      t.references :area, foreign_key: true
      t.datetime :exit_at

      t.timestamps
    end
  end
end
