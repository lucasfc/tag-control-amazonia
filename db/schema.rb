# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190729174910) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "areas", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.integer "limit"
    t.boolean "exit_register"
    t.text "webhook"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "theme_id"
    t.integer "event_id"
    t.string "code"
    t.boolean "private"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "category_areas", force: :cascade do |t|
    t.bigint "category_id"
    t.bigint "area_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["area_id"], name: "index_category_areas_on_area_id"
    t.index ["category_id"], name: "index_category_areas_on_category_id"
  end

  create_table "device_readers", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.string "secrect"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "device_tags", force: :cascade do |t|
    t.string "code"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "param_configs", force: :cascade do |t|
    t.string "namespace"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "people", force: :cascade do |t|
    t.string "name"
    t.string "idnumber"
    t.string "email"
    t.string "phone"
    t.string "company"
    t.string "role"
    t.date "date"
    t.string "mother"
    t.string "gender"
    t.text "photo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "person_categories", force: :cascade do |t|
    t.bigint "person_id"
    t.bigint "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_person_categories_on_category_id"
    t.index ["person_id"], name: "index_person_categories_on_person_id"
  end

  create_table "person_tags", force: :cascade do |t|
    t.bigint "person_id"
    t.bigint "device_tag_id"
    t.datetime "inactivated_at"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["device_tag_id"], name: "index_person_tags_on_device_tag_id"
    t.index ["person_id"], name: "index_person_tags_on_person_id"
  end

  create_table "questions", force: :cascade do |t|
    t.string "code"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "category"
  end

  create_table "replies", force: :cascade do |t|
    t.bigint "question_id"
    t.string "code"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["question_id"], name: "index_replies_on_question_id"
  end

  create_table "reply_people", force: :cascade do |t|
    t.bigint "reply_id"
    t.bigint "person_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["person_id"], name: "index_reply_people_on_person_id"
    t.index ["reply_id"], name: "index_reply_people_on_reply_id"
  end

  create_table "tag_registers", force: :cascade do |t|
    t.bigint "person_tag_id"
    t.bigint "device_reader_id"
    t.bigint "area_id"
    t.datetime "exit_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["area_id"], name: "index_tag_registers_on_area_id"
    t.index ["device_reader_id"], name: "index_tag_registers_on_device_reader_id"
    t.index ["person_tag_id"], name: "index_tag_registers_on_person_tag_id"
  end

  add_foreign_key "category_areas", "areas"
  add_foreign_key "category_areas", "categories"
  add_foreign_key "person_categories", "categories"
  add_foreign_key "person_categories", "people"
  add_foreign_key "person_tags", "device_tags"
  add_foreign_key "person_tags", "people"
  add_foreign_key "replies", "questions"
  add_foreign_key "reply_people", "people"
  add_foreign_key "reply_people", "replies"
  add_foreign_key "tag_registers", "areas"
  add_foreign_key "tag_registers", "device_readers"
  add_foreign_key "tag_registers", "person_tags"
end
